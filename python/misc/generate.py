#########################################################################################
# This is a Python script to generate cytosim .cym file for fiber stretching simulations
# Use: generate.py WRITEFOLDER
# WRITEFOLDER is the name of the folder where the .cym file has to be written
# the file name is defined in the FILE GENERATION PARAMETERS
#
#########################################################################################
# IMPORT MODULES
#python functions
from __future__ import division
import os
import sys
import shutil
import numpy as np
from datetime import date
#
#custom functions
# srcpath = '../'
# sys.path.append(srcpath)
from writeCymFile import writeCymFile
#
#########################################################################################
# FILE GENERATION PARAMETERS
#
#destination folder and file name
writefolder = sys.argv[1]
#create writefolder if it doesn't exist
if not os.path.isdir(writefolder):
	os.mkdir(writefolder)
fname = writefolder + '/config.cym'
#
#file preamble
author_name = 'M. Pensalfini, UPC-BarcelonaTech'
today = date.today()
author_date = '% ' + author_name + ', ' + today.strftime("%d-%B-%Y") + '\n%'
#
header_comm = '%\n% A straight fiber attached to two \'fixtures\' via stiff springs (k_s >> k_f) \n\
% The fiber is stretched up to 100% strain by gradually displacing the  \n\
% fixtures and then completely unloaded. t_load = 10 s and t_step = 1 ms \n%\n'
separator   = '\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n'
#
#########################################################################################
# GLOBAL SIMULATION PARAMETERS
#
#define if fiber unloading has to be included (0/1)
unloading = 1
#
#set applied stretch
stretch_val = 2.0
#
#system temperature (prescribe a value or set it to -1 to use default value: kT = 0.0042 pN*um)
kT_val = -1
# 
#solver preconditioner (0 seems faster for most simulations, details in src/sim/simul_prop.h)
precondition = 0
#
#cytosol viscosity in pN*s/um^2 (prescribe a value or set it to -1 to use default value of 1 pN*s/um^2)
viscosity = -1
#
#set time and step parameters
time_inc          = 1e-3                                                   #simulation time step in s
save_freq         = 100                                                    #saving frequency for whole simulation (intended as: 1 frame every save_freq)      
time_equil_bef    = 1                                                      #equilibration time before stretching (in s)
time_stretch      = 100                                                    #loading time (in s)
time_equil_aft    = 1                                                      #equilibration time after stretching (in s)
#
#space: whole cell
space_name        = 'cell'                                                 #simulation space name
space_shape       = 'sphere'                                               #simulation space shape (typically: sphere)
space_radius      = 50                                                     #simulation space radius (set large enough)
space_displ_opt   = 'display = ( color = none )'                           #display options
#
#########################################################################################
# SPRING PROPERTIES
#the fiber ends are 'clamped' using springs that have finite stiffness
spring_name       = 'spring'     #name
spring_stiff      = 2500         #spring stiffness
#
#########################################################################################
# FIBER PROPERTIES
FIB_name          = 'filament'                                             #fiber name
FIB_length        = 10                                                     #fiber length
FIB_rigidity      = 0.05                                                   #kappa = lp*kT, where kT = 0.0042 pN*um
FIB_segment       = 0.5                                                    #filament segmentation
FIB_constitutive  = [100, 1.25, 50, 10]                                    #mechanical parameters: E1*Af, Tau1, E2*Af, Tau2
FIB_nonlinear     = [50, 500, 0.2, 0.8, 50]                                #mechanical parameters: (E1*Af)_soften, (E1*Af)_stiffen, eps_soften, eps_stiffen, gamma
FIB_displ_opt     = 'display = ( coloring = 1; )'                          #fiber display options
#
#########################################################################################
# WRITE .cym FILE
writeCymFile(fname, separator, header_comm, author_date, unloading, \
             kT_val, viscosity, precondition, \
             space_name, space_shape, space_radius, space_displ_opt, \
             time_inc, time_equil_bef, time_stretch, time_equil_aft, save_freq, \
             stretch_val, spring_name, spring_stiff, \
             FIB_name, FIB_length, FIB_rigidity, FIB_segment, FIB_constitutive, \
             FIB_nonlinear, FIB_displ_opt)
#
#########################################################################################
# FILE ENDS HERE
#