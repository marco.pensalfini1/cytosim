#########################################################################################
# python function to write .cym file for cytosim simulation of fiber stretching
#
import numpy as np
#
def writeCymFile(fname, separator, header_comm, author_date, unloading, \
                 kT_val, viscosity, precondition, \
                 space_name, space_shape, space_radius, space_displ_opt, \
                 time_inc, time_equil_bef, time_stretch, time_equil_aft, save_freq, \
                 stretch_val, spring_name, spring_stiff, \
                 FIB_name, FIB_length, FIB_rigidity, FIB_segment, FIB_constitutive, \
                 FIB_nonlinear, FIB_displ_opt):
    #
    #####################################################################################
    #open file and write header
    f = open(fname,'w+')
    f.write(separator)
    f.write(header_comm + author_date)
    #
    #####################################################################################
    #define global simulation parameters
    f.write(separator)
    f.write('% GLOBAL SIMULATION PARAMETERS \n\n' +\
            'set simul system \n{' +\
                    '\n\t time_step = ' + str(time_inc) )
    if kT_val > 0:
        f.write('\n\t kT = ' + str(kT_val))
    if viscosity > 0:
        f.write('\n\t viscosity = ' + str(viscosity))
    f.write('\n\t precondition = ' + str(precondition))
    # f.write('\n\t ' + glob_display_opt[:-1] + ' line_width=' + str(2.0*IF_radius) + ' )' )
    f.write('\n}\n')
    #
    #####################################################################################
    #define simulation space
    f.write(separator)
    #single fiber tensile test -> space is a spherical cell
    f.write('% SIMULATION SPACE: SPHERE \n\n' +\
            'set space ' + space_name + '\n{' +\
                    '\n\t shape = ' + str(space_shape) +\
                    '\n\t ' + space_displ_opt +\
            '\n}\n')
    f.write('\n' + 'new space ' + space_name + ' {' + ' radius = ' + str(space_radius) + ' }\n' )
    #
    #####################################################################################
    #define fiber using shape
    f.write(separator)
    # define fiber properties
    f.write('% FIBER PROPERTIES\n')
    f.write('\nset fiber ' + str(FIB_name) + '\n{' +\
                    '\n\t rigidity = ' + str(FIB_rigidity) +\
                    '\n\t segmentation = ' + str(FIB_segment) )
    if len(FIB_constitutive) > 0:
        f.write('\n\t activity = stretch')
        f.write('\n\t constitutive = ' + ", ".join(map(str,FIB_constitutive)))
    if len(FIB_nonlinear) > 0:
        f.write('\n\t nonlinear = ' + ", ".join(map(str,FIB_nonlinear)))
    f.write('\n\t ' + FIB_displ_opt + '\n}\n')
    #place fiber in space 
    f.write('new ' + str(FIB_name) + '\n{' +\
                    '\n\t placement = none' +\
                    '\n\t shape = ' + str(-FIB_length/2.) + \
                    ' 0 0, ' + str(FIB_length/2.) + ' 0 0 \n')
    f.write('}\n')
    #
    #####################################################################################
    #define hand and singles to be used as fiber clamps
    f.write(separator)
    f.write('% SPRING PROPERTIES \n\n' +\
            'set hand bind \n{' +\
                    '\n\t unbinding_rate = 0' +\
                    '\n\t unbinding_force = inf' +\
                    '\n\t activity = track' +\
                    '\n\t track_end = both_ends' +\
                    '\n\t bind_only_end = both_ends' +\
                    '\n\t hold_growing_end = 1' +\
                    '\n\t hold_shrinking_end = 1' +\
                    '\n\t display = ( size=8; color=orange )' +\
                    '\n}' )
    f.write('\n\n' +\
            'set single clamp \n{' +\
                    '\n\t hand = bind' +\
                    '\n\t activity = fixed' +\
                    '\n\t stiffness = ' + str(spring_stiff) +\
                    '\n}' )
    #place minus_end clamp
    f.write('\n\n' +\
            'new clamp \n{' +\
                    '\n\t position = ' + str(-FIB_length/2.) + ' 0 0' + \
                    '\n\t attach = fiber, 0, minus_end' +\
                    '\n}' )
    #place plus_end clamp
    f.write('\n\n' +\
            'new clamp \n{' +\
                    '\n\t position = ' + str(FIB_length/2.) + ' 0 0' + \
                    '\n\t attach = fiber, 0, plus_end' +\
                    '\n}\n' )
    #
    #####################################################################################
    #let system equilibrate for a few steps
    f.write(separator)
    f.write('% INITIAL SYSTEM EQUILIBRATION \n\n')
    Nsteps_equil_bef = int(np.round(time_equil_bef/(time_inc)))
    f.write('run ' + str(Nsteps_equil_bef) + ' system { nb_frames = ' + str(int(Nsteps_equil_bef/save_freq)) + ' }\n')
    #
    #####################################################################################
    #run simulation in mutiple steps to progressively stretch up to prescribed value
    f.write(separator)
    f.write('% BEGIN FIBER LOADING \n')
    Nsteps_stretch = int(np.round(time_stretch/(time_inc)))
    strain_vals    = np.linspace(1,stretch_val,Nsteps_stretch+1) 
    for inc in range(1,Nsteps_stretch+1):
        f.write('\n% stretch to ' + str(np.round(strain_vals[inc]*100-100,3)) + '% strain \n')
        f.write('delete all clamp\n')
        f.write('new clamp \n{' +\
                        '\n\t position = ' + str(-FIB_length/2.*strain_vals[inc]) + '0 0' +\
                        '\n\t attach = fiber, 0, minus_end' +\
                        '\n}\n' )
        f.write('new clamp \n{' +\
                        '\n\t position = ' + str(FIB_length/2.*strain_vals[inc]) + '0 0' +\
                        '\n\t attach = fiber, 0, plus_end' +\
                        '\n}\n' )
        if (inc/save_freq).is_integer():
            f.write('run 1 system { nb_frames = 1 }\n')
        else:
            f.write('run 1 system { nb_frames = 0 }\n')
    #
    #finally: progressively unload back to 0 strain
    if unloading:
        f.write(separator)
        f.write('% BEGIN FIBER UNLOADING \n')
        for inc in range(0,Nsteps_stretch)[::-1]:
            f.write('\n% stretch to ' + str(np.round(strain_vals[inc]*100-100,3)) + '% strain \n')
            f.write('delete all clamp\n')
            f.write('new clamp \n{' +\
                        '\n\t position = ' + str(-FIB_length/2.*strain_vals[inc]) + '0 0' +\
                            '\n\t attach = fiber, 0, minus_end' +\
                            '\n}\n' )
            f.write('new clamp \n{' +\
                        '\n\t position = ' + str(FIB_length/2.*strain_vals[inc]) + '0 0' +\
                            '\n\t attach = fiber, 0, plus_end' +\
                            '\n}\n' )
            if (inc/save_freq).is_integer():
                f.write('run 1 system { nb_frames = 1 }\n')
            else:
                f.write('run 1 system { nb_frames = 0 }\n')
    #
    #####################################################################################
    #let system equilibrate for a few steps
    f.write(separator)
    f.write('% FINAL SYSTEM EQUILIBRATION \n\n')
    Nsteps_equil_aft = int(np.round(time_equil_aft/(time_inc)))
    f.write('run ' + str(Nsteps_equil_aft) + ' system { nb_frames = ' + str(int(Nsteps_equil_aft/save_freq)) + ' }\n')
    #
    #####################################################################################
    #close file
    f.close()
    #