// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// PlectinLamin type for Single was implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-23)
/*
   This class is based on the Picket type and is used to model a single that is rigidly anchored to the cell nucleus.
*/
#include "plectin_lamin.h"
#include "simul.h"
#include "meca.h"
#include "modulo.h"


extern Modulo const* modulo;


PlectinLamin::PlectinLamin(SingleProp const* p, Vector const& w)
: Single(p, w)
{
#if ( 0 )
    if ( p->diffusion > 0 )
        throw InvalidParameter("single:diffusion cannot be > 0 if activity=fixed");
#endif
}


PlectinLamin::~PlectinLamin()
{
    //std::clog<<"~PlectinLamin("<<this<<")"<<std::endl;
}


void PlectinLamin::beforeDetachment(Hand const*)
{
    assert_true( attached() );

    SingleSet * set = static_cast<SingleSet*>(objset());
    if ( set )
        set->relinkD(this);
}


void PlectinLamin::stepF(Simul& sim)
{
    assert_false( sHand->attached() );

    // read nucleus and check if it exists
    Mecable * nucleus = simul().findMecable("cell_nucleus");

    if ( nucleus )
    {
        // append nucleus position to posN array
        posN.push_back(nucleus->posPoint(0));

        // except for first step, move single rigidly with nucleus
        if ( posN.size() > 1 )
            translate(posN[posN.size()-1] - posN[posN.size()-2]);
    }

    sHand->stepUnattached(sim, sPos);
}


void PlectinLamin::stepA()
{
    assert_true( sHand->attached() );

    // read nucleus and check if it exists
    Mecable * nucleus = simul().findMecable("cell_nucleus");

    if ( nucleus )
    {
        // append nucleus position to posN array
        posN.push_back(nucleus->posPoint(0));

        // except for first step, move single rigidly with nucleus
        if ( posN.size() > 1 )
            translate(posN[posN.size()-1] - posN[posN.size()-2]);
    }

    Vector f = force();
    sHand->stepLoaded(f, f.norm());
}


/**
 This calculates the force corresponding to addPointClamp()
 */
Vector PlectinLamin::force() const
{
    assert_true( sHand->attached() );
    Vector d = sPos - posHand();
    
    if ( modulo )
        modulo->fold(d);
    
    return prop->stiffness * d;
}


void PlectinLamin::setInteractions(Meca & meca) const
{
    assert_true( prop->length == 0 );

    // apply interaction to relevant point on meca
    meca.addPointClamp(sHand->interpolation(), sPos, prop->stiffness);
    //meca.addLineClamp(sHand->interpolation(), sPos, sHand->dirFiber(), prop->stiffness);

    // apply equal and opposed force to nucleus
    Mecable * nucleus  = simul().findMecable("cell_nucleus");
    if ( nucleus )
    {
        const index_t inxN = DIM * nucleus->matIndex();
        Vector    forceN   = force();
        meca.base(inxN  ) -= forceN[0];
        meca.base(inxN+1) -= forceN[1];
        meca.base(inxN+2) -= forceN[2];
    }
}


