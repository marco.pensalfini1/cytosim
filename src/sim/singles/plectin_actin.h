// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// PlectinActin type for Single was implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-23)
/*
   This class is based on the Picket type and is used to model a single that is anchored to the bounding
   space. The anchoring point is obtained from projecting the PlectinActin position to the space boundary,
   and it moves affinely with it when the space boundary is deformed.
*/

#ifndef PLECTINACTIN_H
#define PLECTINACTIN_H

#include "single.h"


/// a Single attached at a fixed position.
/**
 This Single is fixed at the position determined from projectin it on the bounding space.
 A link is created if the Hand is attached.

 @ingroup SingleGroup
 */
class PlectinActin : public Single
{
public:
    
    /// sPos should never change
    void    beforeDetachment(Hand const*);
    /// stiffness of the interaction
    real    interactionStiffness() const { return prop->stiffness; }

public:

    /// constructor
    PlectinActin(SingleProp const*, Vector const& = Vector(0,0,0));

    /// destructor
    ~PlectinActin();
    
    ///return the position in space of the object
    Vector  position()           const  { return sPos; }

    /// PlectinActin accepts translation
    int     mobile()             const  { return 1; }
    
    /// translate object's position by the given vector
    void    translate(Vector const& w)  { sPos += w; }
    
    /// true if Single creates an interaction
    bool    hasForce() const { return true; }

    /// tension in the link = stiffness * ( posFoot() - posHand() )
    Vector  force() const;

    /// Monte-Carlo step for a free Single
    void    stepF(Simul&);
    
    /// Monte-Carlo step for a bound Single
    void    stepA();
    
    /// add interactions to a Meca
    void    setInteractions(Meca &) const;
    
};


#endif
