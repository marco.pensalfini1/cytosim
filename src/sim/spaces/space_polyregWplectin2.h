// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// SpacePolyRegWPlectin2 class implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-23)
/*
   This class is based on the custom implementation SpacePolyReg. It is used to model a right 
   regular prism whose faces represent the cell actin cortex, which is rich in plectin linkers 
   whose characteristics can be prescribed separately using dedicated singles.
   The prism is defined by prescribing its geometrical characteristics (height, number of sides
   in the basis, and radius of circle inscribing the basis), as well as the rate of equibiaxial
   strain applied to the cell and an equilibration time before loading.
   Note that lenghts are measured in the reference configuration and the hole position is updated
   in order to account for fiber-hole contacts.
*/

#ifndef SPACE_POLYREGWPLECTIN2_H
#define SPACE_POLYREGWPLECTIN2_H

#include "space.h"
#include "polygon.h"

/// a right regular prism of axis Z with top and bottom caps
/**
 Space `polyregWplectin2` is the extrusion of a regular polygon along the Z-axis to reach a height `height`.
 The cross section in the XY plane is a regular polygon of `nsides` sides.
 The top and bottom polygons are part of the surface.
 project() will always project:
 i)   to the surface of the prism wall that corresponds to the sector where the mecapoint is found,
      the sectors are obtained by connecting the center of the polygonal base with the nsides vertices
 ii)  to the top/bottom prism face, if the meca point is above/below the prism height along Z
 iii) to the outer shell of the cylindrical nucleus, if the meca point is inside the nucleus

 Parameters:
     - nsides   = number of equal length sides for the polygonal base
     - radius0  = radius of the inscribing circle used to construct the polygonal base, measured in the reference configuration
     - height0  = total extension of the prism in Z, measured in the reference configuration
     - t_equil  = duration of equilibration phase prior to loading
     - eps_rate = rate of applied areal strain, assuming equibiaxial deformation and setting F33 automatically based on `isochoric`
     - hole_rad = radius of the cylinder representing the nucleus in 2.5D
     - hole_pos = position of the center of the cylinder representing the nucleus in 2.5D

 @ingroup SpaceGroup
 */
class SpacePolyRegWPlectin2 : public Space
{
    /// apply a force directed towards the edge of the Space
    static void setInteraction(Vector const& pos, Mecapoint const&, Meca &, real stiff, const real len, const real apo, const real nsid, const real Fval[] );

private:
    
    /// The regular polygon object used to obtain the prism
    Polygon  poly_;

    /// the number of sides of the prism polygonal base
    real     nsides_;

    /// the radius of the inscribing circle used to build the prism base
    real     radius0_;

    /// half the reference height of the prism
    real     height0_;

    /// cell equilibration time before and after loading
    real     t_equil_;

    /// applied cell areal strain rate (assumed equibiaxial)
    real     eps_rate_;

    /// the radius of the cylindrical hole representing the nucleus in 2.5D
    real     hole_rad_;

    /// the position of the cylindrical hole representing the nucleus in 2.5D
    Vector2  mutable hole_pos_;

    /// Surface of polygon
    real     surface_;

    /// calculate apothem_ and side_
    void     update();

public:
        
    ///constructor
    SpacePolyRegWPlectin2(const SpaceProp *);
    
    /// destructor
    ~SpacePolyRegWPlectin2();

    /// change dimensions
    void        resize(Glossary& opt);

    /// the volume inside
    // real        volume() const { return ( DIM>2 ? 2*(height0_*Fvals_[4]) : 1 ) * surface_; }

    /// number of sides in the polygon
    int         nsides() const { return nsides_;}

    /// current prism height
    // real        height() const { return 2*height0_*Fvals_[4];}

    /// radius of the polygon's inscribing circle
    real        radius() const { return radius0_;}

    /// return current position of hole center
    Vector2     holePos() const { return hole_pos_;}

    /// return current value of hole radius
    real        holeR() const { return hole_rad_;}

    /// true if the point is inside the Space
    bool        inside(Vector const&) const;

    /// a random position inside the volume
    Vector      randomPlace() const;

    /// set `proj` as the point on the edge that is closest to `point`
    Vector      project(Vector const& pos) const;

    /// set a specific `proj` type for plectin_actin, bringing singles on the cell boundary and imposing an affine deformation
    Vector      projectPlectinActin(Vector const& pos) const;

    /// apply a force directed towards the edge of the Space for any meca point
    void        setInteraction(Vector const& pos, Mecapoint const&, Meca &, real stiff) const;

    /// apply a force directed towards the edge of the Space for any point within a distance `inter_rad` from cell walls
    void        setInteractionLinIns(Vector const& pos, Mecapoint const&, Meca &, real stiff, real inter_rad) const;

    /// apply a force directed towards the edge of the Space to ensure that fiber ends slide on closest cell wall
    void        setInteractionFibEnds(Vector const& pos, Mecapoint const&, Meca &, real stiff) const;

    /// apply a force directed towards the edge of the Space to constrain fiber ends on the cell wall where they were initially
    void        setInteractionFibEnds(Vector const& pos, Vector const& pos0, Mecapoint const&, Meca &, real stiff) const;

    /// apply a force directed towards the initial position (pos0) of a fiber end located at `pos`
    void        setInteractionFibEndsFixed(Vector const& pos, Vector const& pos0, Mecapoint const&, Meca &, real stiff) const;
    
    /// OpenGL display function; returns true if successful
    bool        draw() const;
    
    /// write to file
    void        write(Outputter&) const;

    /// get dimensions from array `len`
    void        setLengths(const real len[8]);
    
    /// read from file
    void        read(Inputter&, Simul&, ObjectTag);

};

#endif

