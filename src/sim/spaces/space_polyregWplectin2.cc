// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// SpacePolyRegWPlectin2 class implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-23)
/*
   This class is based on the custom implementation SpacePolyReg. It is used to model a right 
   regular prism whose faces represent the cell actin cortex, which is rich in plectin linkers 
   whose characteristics can be prescribed separately using dedicated singles.
   The prism is defined by prescribing its geometrical characteristics (height, number of sides
   in the basis, and radius of circle inscribing the basis), as well as the rate of equibiaxial
   strain applied to the cell and an equilibration time before loading.
   Note that lenghts are measured in the reference configuration and the hole position is updated
   in order to account for fiber-hole contacts.
*/

#include "dim.h"
#include "space_polyregWplectin2.h"
#include "exceptions.h"
#include "mecapoint.h"
#include "iowrapper.h"
#include "glossary.h"
#include "polygon.h"
#include "meca.h"
#include "fiber.h"
#include "fiber_segment.h"
#include "fiber_prop.h"
#include "simul.h"
#include <fstream>

SpacePolyRegWPlectin2::SpacePolyRegWPlectin2(SpaceProp const* p)
: Space(p)
{
    if ( DIM < 3 )
        throw InvalidParameter("polyregWplectin2 is currently only implemented in 3D");
    nsides_      = 3;
    radius0_     = 1;
    height0_     = 1;
    t_equil_     = 0;
    eps_rate_    = 0;
    hole_rad_    = 0;
    hole_pos_[0] = 0;
    hole_pos_[1] = 0;
}

SpacePolyRegWPlectin2::~SpacePolyRegWPlectin2()
{
}

void SpacePolyRegWPlectin2::resize(Glossary& opt)
{
    real nsid = nsides_, rad = radius0_, t_eq = t_equil_, eps_dot = eps_rate_, holR = hole_rad_;

    // read and set prism basis parameters
    opt.set(nsid, "nsides");
    if ( opt.set(rad, "diameter") )
        rad *= 0.5;
    else if ( opt.set(rad, "radius") )
        opt.set(rad, "radius");
    else if ( opt.set(rad, "side") )
        rad /= 2*std::sin(M_PI/nsid);
    else if ( opt.set(rad, "apothem") )
        rad /= std::cos(M_PI/nsid);

    if ( nsid < 3 )
        throw InvalidParameter("polyregWplectin2:number of sides must be >= 3");
    if ( rad < 0 )
        throw InvalidParameter("polyregWplectin2:radius must be >= 0");

    nsides_ = nsid;
    radius0_ = rad;

    // read and set cylindrical hole parameters
    if ( opt.set(holR, "hole_diameter") )
        holR *= 0.5;
    else if ( opt.set(holR, "hole_radius") )
        opt.set(holR, "radius");

    if ( holR < 0 )
        throw InvalidParameter("polyregWplectin2:hole radius must be >= 0");

    hole_rad_ = holR;

    for ( int d = 0; d < 2; ++d )
    {
        real posH = hole_pos_[d];
        opt.set(posH, "hole_position", d);
        hole_pos_[d] = posH;
    }

    // read and set loading parameters
    opt.set(t_eq, "equilibrate");
    opt.set(eps_dot, "strain_rate");

    if ( t_eq < 0 )
        throw InvalidParameter("polyregWplectin2:equilibration time must be >= 0");
    if ( eps_dot < 0 )
        throw InvalidParameter("polyregWplectin2:strain rate must be >= 0");

    t_equil_ = t_eq;
    eps_rate_ = eps_dot;

#if ( DIM == 3 )
    // read and set prism height parameters
    real len = height0_;

    if ( opt.set(len, "height") )
        len *= 0.5;

    if ( len < 0 )
        throw InvalidParameter("polyregWplectin2:height must be >= 0");

    height0_ = len;
#endif

    update();
}

void SpacePolyRegWPlectin2::update()
{
    // define polygon object:
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_ref[nbp+1];
    real edge_angles[nbp+1];

    poly_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_ref[p][0]    = radius0_ * cos( p * sector_angle_ref );
        x_ref[p][1]    = radius0_ * sin( p * sector_angle_ref );
        poly_.setPoint(p, x_ref[p][0], x_ref[p][1]);
    }
    poly_.complete(REAL_EPSILON);

    surface_ = poly_.surface();
    if ( surface_ < 0 )
        poly_.flip();

    assert_true( surface_ > 0 );
    
    if ( poly_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

}

bool SpacePolyRegWPlectin2::inside(Vector const& w) const
{
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    // define polygon object in the current configuration:
    Polygon poly_curr_;
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_curr_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_curr[p][0]   = std::sqrt(lambdaXY_curr) * radius0_ * cos( p * sector_angle_ref );
        x_curr[p][1]   = std::sqrt(lambdaXY_curr) * radius0_ * sin( p * sector_angle_ref );
        poly_curr_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_curr_.complete(REAL_EPSILON);

    real surface_curr_ = poly_curr_.surface();
    if ( surface_curr_ < 0 )
        poly_curr_.flip();

    assert_true( surface_curr_ > 0 );
    
    if ( poly_curr_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

// only check if mecapoint is within prism height if DIM > 2
#if ( DIM > 2 )
    if ( hole_rad_ == 0 ) // if no hole, consider no top plate
    {
        if ( w.ZZ < -height0_ * lambdaZ_curr )
            return false;
    }
    else // if there is a cylindrical hole, consider both top and bottom plates
    {
        if ( fabs(w.ZZ) > height0_ * lambdaZ_curr )
            return false;
    }
#endif

// in all cases, check if mecapoint is within deformed hexagonal basis but outside cylindrical hole
#if ( DIM > 1 )

    bool in_poly = poly_curr_.inside(w.XX, w.YY, 1);
    bool in_hole = false;

    if ( hole_rad_ > 0 )
        in_hole = (w - hole_pos_).normXY() * (w - hole_pos_).normXY() <= hole_rad_ * hole_rad_;

    return in_poly * !in_hole;

#else
    return false;
#endif
}

Vector SpacePolyRegWPlectin2::randomPlace() const
{
    if ( surface_ <= 0 )
        throw InvalidParameter("cannot pick point inside polygon of null surface");
    return Space::randomPlace();
}

//------------------------------------------------------------------------------
/**
 Check if point (w.XX, w.YY, w.ZZ) is inside polygon
 */
Vector SpacePolyRegWPlectin2::project(Vector const& w) const
{
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    // define polygon object in the current configuration:
    Polygon poly_curr_;
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_curr_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_curr[p][0]   = std::sqrt(lambdaXY_curr) * radius0_ * cos( p * sector_angle_ref );
        x_curr[p][1]   = std::sqrt(lambdaXY_curr) * radius0_ * sin( p * sector_angle_ref );
        poly_curr_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_curr_.complete(REAL_EPSILON);

    real surface_curr_ = poly_curr_.surface();
    if ( surface_curr_ < 0 )
        poly_curr_.flip();

    assert_true( surface_curr_ > 0 );
    
    if ( poly_curr_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

    Vector p;
#if ( DIM == 1 )
    
    p.XX = w.XX;
    
#elif ( DIM == 2 )
    
    int hit;
    poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
    
#elif ( DIM > 2 )
    
    if ( hole_rad_ == 0)
    {
        // if ( fabs(w.ZZ) > height0_ * lambdaZ_curr )
        if ( w.ZZ < -height0_ * lambdaZ_curr )
        {
            if ( poly_curr_.inside(w.XX, w.YY, 1) )
            {
                // too high or too low in the Z axis, but inside XY
                p.XX = w.XX;
                p.YY = w.YY;
            }
            else
            {
                // outside in Z and XY
                int hit;
                poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
            }
            // p.ZZ = std::copysign(height0_ * lambdaZ_curr, w.ZZ);
            p.ZZ = -height0_ * lambdaZ_curr;
        }
        else
        {
            int hit;
            poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
            if ( poly_curr_.inside(w.XX, w.YY, 1) )
            {
                // inside in the Z axis and the XY polygon:
                // to the polygonal edge in XY plane:
                real hh = (w.XX-p.XX) * (w.XX-p.XX) + (w.YY-p.YY) * (w.YY-p.YY);
                // // to the top/bottom plates:
                // real v = height0_ * lambdaZ_curr - fabs(w.ZZ);
                // // compare distances
                // if ( v * v < hh )
                //     return Vector(w.XX, w.YY, std::copysign(height0_ * lambdaZ_curr, w.ZZ));
                // to the bottom plate (since top plate can be crossed):
                real v = -height0_ * lambdaZ_curr - w.ZZ;
                // compare distances
                if ( v * v < hh )
                    return Vector(w.XX, w.YY, -height0_ * lambdaZ_curr);
            }
            p.ZZ = w.ZZ;
        }
    }
    else
    {
        bool in_hole = (w - hole_pos_).normXY() * (w - hole_pos_).normXY() <= hole_rad_ * hole_rad_;
        Vector pH    = hole_pos_ + hole_rad_ * (w - hole_pos_) / (w - hole_pos_).normXY();

        if ( fabs(w.ZZ) > height0_ * lambdaZ_curr )
        {
            if ( poly_curr_.inside(w.XX, w.YY, 1) ) // too high or too low in the Z axis, but inside XY
            {
                // if point w is inside cylindrical hole, project XY coords to hole's wall
                if ( in_hole )
                {
                    p.XX = pH.XX;
                    p.YY = pH.YY;
                }
                // if point w is not inside cylindrical hole, don't touch its XY coords             
                else
                {
                    p.XX = w.XX;
                    p.YY = w.YY;                    
                }                
            }
            else // outside in Z and also in XY
            {
                int hit;
                poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
            }
            p.ZZ = std::copysign(height0_ * lambdaZ_curr, w.ZZ); // Z coord is projected to top/bottom plate
        }
        else
        {
            int hit;
            poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
            if ( poly_curr_.inside(w.XX, w.YY, 1) )
            {
                // if point w is inside cylindrical hole, project to hole's wall
                if ( in_hole )
                {
                    p.XX = pH.XX;
                    p.YY = pH.YY;
                }
                // if point w is not inside cylindrical hole, check which wall is the closest            
                else
                {
                    // measure XY distance to the nearest prism side wall:
                    real hh = (w.XX-p.XX) * (w.XX-p.XX) + (w.YY-p.YY) * (w.YY-p.YY);
                    // measure XY distance to the hole's cylindrical wall:
                    real rr = (w.XX-pH.XX) * (w.XX-pH.XX) + (w.YY-pH.YY) * (w.YY-pH.YY);
                    // measure distance to the top/bottom plates:
                    real v = height0_ * lambdaZ_curr - fabs(w.ZZ);
                    // compare distances
                    if ( v * v < hh && v * v < rr )
                        return Vector(w.XX, w.YY, std::copysign(height0_ * lambdaZ_curr, w.ZZ));
                }
            }
            p.ZZ = w.ZZ;            
        }
    }
    
#endif
    return p;
}

Vector SpacePolyRegWPlectin2::projectPlectinActin(Vector const& w) const
{
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    // quantify stretch at previous time step
    real lambdaXY_prev = eps_rate_ * ( simul().time() - simul().time_step() - t_equil_ ) + 1.0;
    if ( lambdaXY_prev < 1.0 )
        lambdaXY_prev  = 1.0;
    real lambdaZ_prev  = 1.0;

    // quantify stretch from previous to current time step
    real lambdaXY_step = lambdaXY_curr / lambdaXY_prev;
    real lambdaZ_step  = lambdaZ_curr / lambdaZ_prev;

    // define polygon object in the current configuration:
    Polygon poly_curr_;
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_curr_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_curr[p][0]   = std::sqrt(lambdaXY_curr) * radius0_ * cos( p * sector_angle_ref );
        x_curr[p][1]   = std::sqrt(lambdaXY_curr) * radius0_ * sin( p * sector_angle_ref );
        poly_curr_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_curr_.complete(REAL_EPSILON);

    real surface_curr_ = poly_curr_.surface();
    if ( surface_curr_ < 0 )
        poly_curr_.flip();

    assert_true( surface_curr_ > 0 );
    
    if ( poly_curr_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

    Vector p;

#if ( DIM == 2 )
    
    int hit;
    poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
    
#elif ( DIM > 2 )
    
    if ( hole_rad_ == 0 )
    {
        // case 1: w is not inside the prism in Z
        if ( fabs(w.ZZ) >= height0_ * lambdaZ_curr )
        {
            // case 1a: w XY projection falls inside prism basis -> deform affinely in both X and Y
            if ( poly_curr_.inside(w.XX, w.YY, 1) )
            {
                p.XX = w.XX * std::sqrt(lambdaXY_step);
                p.YY = w.YY * std::sqrt(lambdaXY_step);
            }
            // case 1b: w XY projection falls outside of prism basis -> bring w on nearest side wall
            else
            {
                int hit;
                poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
            }
            // in both cases, bring w on the nearest between the prism top and bottom face
            p.ZZ = std::copysign(height0_ * lambdaZ_curr, w.ZZ);
        }
        // case 2: w is inside the prism in Z
        else
        {
            int hit;
            poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
            // case 2a: w XY projection falls inside prism basis -> deform affinely in X, Y, and Z
            if ( poly_curr_.inside(w.XX, w.YY, 1) )
            {
                p.XX = w.XX * std::sqrt(lambdaXY_step);
                p.YY = w.YY * std::sqrt(lambdaXY_step);
                p.ZZ = w.ZZ * lambdaZ_step;                
            }
            // case 2b: w XY projection falls outside of prism basis -> bring w on nearest side wall and deform affinely in Z
            else
                p.ZZ = w.ZZ * lambdaZ_step;
        }
    }
    else
    {
        // case 1: w is not inside the prism in Z
        if ( fabs(w.ZZ) >= height0_ * lambdaZ_curr )
        {
            // case 1a: w XY projection falls inside prism basis -> deform affinely in both X and Y
            if ( poly_curr_.inside(w.XX, w.YY, 1) )
            {
                // measure XY distance to the hole's cylindrical wall:
                Vector pH = hole_pos_ + hole_rad_ * (w - hole_pos_) / (w - hole_pos_).normXY();
                real rr   = (w.XX-pH.XX) * (w.XX-pH.XX) + (w.YY-pH.YY) * (w.YY-pH.YY);
                // measure XY distance to the nearest prism side wall:
                real hh   = (w.XX-p.XX) * (w.XX-p.XX) + (w.YY-p.YY) * (w.YY-p.YY);
                // compare distances
                if ( rr < hh )
                {
                    p.XX  = pH.XX;
                    p.YY  = pH.YY;
                }
                else
                {
                    p.XX  = w.XX * std::sqrt(lambdaXY_step);
                    p.YY  = w.YY * std::sqrt(lambdaXY_step);
                }
            }
            // case 1b: w XY projection falls outside of prism basis -> bring w on nearest side wall
            else
            {
                int hit;
                poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
            }
            // in both cases, bring w on the nearest between the prism top and bottom face
            p.ZZ = std::copysign(height0_ * lambdaZ_curr, w.ZZ);
        }
        // case 2: w is inside the prism in Z
        else
        {
            int hit;
            poly_curr_.project(w.XX, w.YY, p.XX, p.YY, hit);
            // case 2a: w XY projection falls inside prism basis -> deform affinely in X, Y, and Z
            if ( poly_curr_.inside(w.XX, w.YY, 1) )
            {
                // measure XY distance to the hole's cylindrical wall:
                Vector pH = hole_pos_ + hole_rad_ * (w - hole_pos_) / (w - hole_pos_).normXY();
                real rr   = (w.XX-pH.XX) * (w.XX-pH.XX) + (w.YY-pH.YY) * (w.YY-pH.YY);
                // measure XY distance to the nearest prism side wall:
                real hh   = (w.XX-p.XX) * (w.XX-p.XX) + (w.YY-p.YY) * (w.YY-p.YY);
                // compare distances
                if ( rr < hh )
                {
                    p.XX  = pH.XX;
                    p.YY  = pH.YY;
                }
                else
                {
                    p.XX  = w.XX * std::sqrt(lambdaXY_step);
                    p.YY  = w.YY * std::sqrt(lambdaXY_step);
                }
                p.ZZ = w.ZZ * lambdaZ_step;                
            }
            // case 2b: w XY projection falls outside of prism basis -> bring w on nearest side wall and deform affinely in Z
            else
                p.ZZ = w.ZZ * lambdaZ_step;
        }
    }
    
#endif
    return p;
}

//------------------------------------------------------------------------------

/// interactions to be applied to all meca points
void SpacePolyRegWPlectin2::setInteraction(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff) const
{
#if ( DIM > 1 )
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    // define polygon object in the current configuration:
    Polygon poly_curr_;
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_curr_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_curr[p][0]   = std::sqrt(lambdaXY_curr) * radius0_ * cos( p * sector_angle_ref );
        x_curr[p][1]   = std::sqrt(lambdaXY_curr) * radius0_ * sin( p * sector_angle_ref );
        poly_curr_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_curr_.complete(REAL_EPSILON);

    real surface_curr_ = poly_curr_.surface();
    if ( surface_curr_ < 0 )
        poly_curr_.flip();

    assert_true( surface_curr_ > 0 );
    
    if ( poly_curr_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

    index_t inx = DIM * pe.matIndex();
    
    int hit;
    real pX, pY;
    int edg =  poly_curr_.project(pos.XX, pos.YY, pX, pY, hit);
    real nX = -poly_curr_.pts_[hit].dy;
    real nY =  poly_curr_.pts_[hit].dx;
    
#if ( DIM > 2 )
    bool in = poly_curr_.inside(pos.XX, pos.YY, 1);

    if ( hole_rad_ == 0 )
    {
        // if ( fabs(pos.ZZ) >= height0_ * lambdaZ_curr )
        if ( pos.ZZ <= -height0_ * lambdaZ_curr )
        {
            // meca.mC(inx+2, inx+2) -= stiff;
            // meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      -= stiff * height0_ * lambdaZ_curr;
            if ( in ) return;
        }
        else if ( in )
        {
            // Compare distance to top/bottom plate:
            // real v = height0_ * lambdaZ_curr - fabs(pos.ZZ);
            real v = -height0_ * lambdaZ_curr - pos.ZZ;
            // and distance to polygonal edge in XY plane:
            real hh = (pos.XX-pX) * (pos.XX-pX) + (pos.YY-pY) * (pos.YY-pY);
            
            if ( v * v < hh )
            {
                // meca.mC(inx+2, inx+2) -= stiff;
                // meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
                meca.mC(inx+2, inx+2) -= stiff;
                meca.base(inx+2)      -= stiff * height0_ * lambdaZ_curr;
                return;
            }
        }
    }
    else
    {
        bool in_hole = (pos - hole_pos_).normXY() * (pos - hole_pos_).normXY() <= hole_rad_ * hole_rad_;
        Vector pH    = hole_pos_ + hole_rad_ * (pos - hole_pos_) / (pos - hole_pos_).normXY();

        if ( fabs(pos.ZZ) >= height0_ * lambdaZ_curr )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
            if ( in * !in_hole ) return;
        }
        else if ( in )
        {
            // Compare distance to top/bottom plate:
            real v = height0_ * lambdaZ_curr - fabs(pos.ZZ);
            // and distance to polygonal edge in XY plane:
            real hh = (pos.XX-pX) * (pos.XX-pX) + (pos.YY-pY) * (pos.YY-pY);
            // and distance to hole wall:
            real rr = (pos.XX-pH.XX) * (pos.XX-pH.XX) + (pos.YY-pH.YY) * (pos.YY-pH.YY);

            if ( v * v < hh && v * v < rr )
            {
                meca.mC(inx+2, inx+2) -= stiff;
                meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
                return;
            }
        }
    }
#endif

    if ( hole_rad_ == 0 )
    {
        if ( edg )
        {
            // projection on an edge of normal (nX, nY) already normalized
            const real pr = ( pX * nX + pY * nY ) * stiff;
            
            meca.mC(inx  , inx  ) -= nX * nX * stiff;
            meca.mC(inx  , inx+1) -= nX * nY * stiff;
            meca.mC(inx+1, inx+1) -= nY * nY * stiff;
            
            meca.base(inx  )  += nX * pr;
            meca.base(inx+1)  += nY * pr;
        }
        else
        {
            // projection on a vertex:
#if ( DIM == 2 )
            meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
            meca.mC(inx,   inx  ) -= stiff;
            meca.mC(inx+1, inx+1) -= stiff;
#endif
            meca.base(inx  )  += stiff * pX;
            meca.base(inx+1)  += stiff * pY;
        }
    }
#endif
    else
    {
        bool in_hole = (pos - hole_pos_).normXY() * (pos - hole_pos_).normXY() <= hole_rad_ * hole_rad_;
        Vector pH    = hole_pos_ + hole_rad_ * (pos - hole_pos_) / (pos - hole_pos_).normXY();
        if ( in_hole )
        {
#if ( DIM == 2 )
            meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
            meca.mC(inx,   inx  ) -= stiff;
            meca.mC(inx+1, inx+1) -= stiff;
#endif
            meca.base(inx  )  += stiff * pH.XX;
            meca.base(inx+1)  += stiff * pH.YY;

            // displace hole according to force applied by point `pos`, considering the
            // drag of a sphere with same radius as the hole (see fiber.cc, line 568)
            real drag_sphere = 6 * M_PI * simul().viscosity() * hole_rad_;

            Vector force = stiff * (pos - pH);

            real vX_hole = force.XX / drag_sphere;
            real vY_hole = force.YY / drag_sphere;

            real dX_hole = vX_hole * simul().time_step();
            real dY_hole = vY_hole * simul().time_step();

            hole_pos_[0] += dX_hole;
            hole_pos_[1] += dY_hole;

        }
        else
        {
            if ( edg )
            {
                // projection on an edge of normal (nX, nY) already normalized
                const real pr = ( pX * nX + pY * nY ) * stiff;
                
                meca.mC(inx  , inx  ) -= nX * nX * stiff;
                meca.mC(inx  , inx+1) -= nX * nY * stiff;
                meca.mC(inx+1, inx+1) -= nY * nY * stiff;
                
                meca.base(inx  )  += nX * pr;
                meca.base(inx+1)  += nY * pr;
            }
            else
            {
                // projection on a vertex:
#if ( DIM == 2 )
                meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
                meca.mC(inx,   inx  ) -= stiff;
                meca.mC(inx+1, inx+1) -= stiff;
#endif
                meca.base(inx  )  += stiff * pX;
                meca.base(inx+1)  += stiff * pY;
            }
        }
    }
}

/// interactions to be applied to any point falling within a distance `inter_rad` from cell walls
void SpacePolyRegWPlectin2::setInteractionLinIns(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff, real inter_rad) const
{
#if ( DIM > 1 )
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    // define polygon object in the current configuration:
    Polygon poly_curr_;
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_curr_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_curr[p][0]   = std::sqrt(lambdaXY_curr) * radius0_ * cos( p * sector_angle_ref );
        x_curr[p][1]   = std::sqrt(lambdaXY_curr) * radius0_ * sin( p * sector_angle_ref );
        poly_curr_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_curr_.complete(REAL_EPSILON);

    real surface_curr_ = poly_curr_.surface();
    if ( surface_curr_ < 0 )
        poly_curr_.flip();

    assert_true( surface_curr_ > 0 );
    
    if ( poly_curr_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

    index_t inx = DIM * pe.matIndex();
    
    int hit;
    real pX, pY;
    int edg   =  poly_curr_.project(pos.XX, pos.YY, pX, pY, hit);
    real dX   =  pos.XX - pX;
    real dY   =  pos.YY - pY;
    real nX   = -poly_curr_.pts_[hit].dy;
    real nY   =  poly_curr_.pts_[hit].dx;
    real dNdN =  dX * nX + dY * nY;


    if ( edg && ( dNdN < inter_rad * inter_rad ) )
    {
        // projection on an edge of normal (nX, nY) already normalized
        const real pr = ( pX * nX + pY * nY ) * stiff;
        
        meca.mC(inx  , inx  ) -= nX * nX * stiff;
        meca.mC(inx  , inx+1) -= nX * nY * stiff;
        meca.mC(inx+1, inx+1) -= nY * nY * stiff;
        
        meca.base(inx  )  += nX * pr;
        meca.base(inx+1)  += nY * pr;
#if ( DIM > 2 )
        if ( hole_rad_ == 0 )
        {
            // if ( fabs(pos.ZZ) >= height0_ * lambdaZ_curr )
            if ( pos.ZZ <= -height0_ * lambdaZ_curr )
            {
                // meca.mC(inx+2, inx+2) -= stiff;
                // meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
                meca.mC(inx+2, inx+2) -= stiff;
                meca.base(inx+2)      -= stiff * height0_ * lambdaZ_curr;
                // if ( in ) return;
            }
        }
        else
        {
            if ( fabs(pos.ZZ) >= height0_ * lambdaZ_curr )
            {
                meca.mC(inx+2, inx+2) -= stiff;
                meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
                // if ( in ) return;
            }
        }
#endif
    }
    else
    {
        // projection on a vertex:
        if ( dNdN < inter_rad * inter_rad )
        {
            // projection on a vertex:
#if ( DIM == 2 )
            meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
            meca.mC(inx,   inx  ) -= stiff;
            meca.mC(inx+1, inx+1) -= stiff;
#endif
            meca.base(inx  )  += stiff * pX;
            meca.base(inx+1)  += stiff * pY;
        }
    }
#endif
}

/// interactions to be applied only to fiber ends, we consider 3 cases:
/// 1) each fiber end point is to be maintained on the closest side wall of the enclosing cell, but can slide on it
/// 2) each fiber end point is to be maintained on the side wall where it is initially found, but can slide on it
/// 3) each fiber end point is to be maintained fixed at its initial position

/// CASE 1: fiber end can slide on closest cell wall (changing wall is allowed)
void SpacePolyRegWPlectin2::setInteractionFibEnds(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff) const
{
#if ( DIM > 1 )
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    // define polygon object in the current configuration:
    Polygon poly_curr_;
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_curr_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_curr[p][0]   = std::sqrt(lambdaXY_curr) * radius0_ * cos( p * sector_angle_ref );
        x_curr[p][1]   = std::sqrt(lambdaXY_curr) * radius0_ * sin( p * sector_angle_ref );
        poly_curr_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_curr_.complete(REAL_EPSILON);

    real surface_curr_ = poly_curr_.surface();
    if ( surface_curr_ < 0 )
        poly_curr_.flip();

    assert_true( surface_curr_ > 0 );
    
    if ( poly_curr_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

    index_t inx = DIM * pe.matIndex();

    int hit;
    real pX, pY;
    int edg =  poly_curr_.project(pos.XX, pos.YY, pX, pY, hit);
    real nX = -poly_curr_.pts_[hit].dy;
    real nY =  poly_curr_.pts_[hit].dx;

    if ( edg )
    {
        // projection on an edge of normal (nX, nY) already normalized
        const real pr = ( pX * nX + pY * nY ) * stiff;

        meca.mC(inx  , inx  ) -= nX * nX * stiff;
        meca.mC(inx  , inx+1) -= nX * nY * stiff;
        meca.mC(inx+1, inx+1) -= nY * nY * stiff;

        meca.base(inx  )  += nX * pr;
        meca.base(inx+1)  += nY * pr;
#if ( DIM > 2 )
        if ( fabs(pos.ZZ) >= height0_ * lambdaZ_curr )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
            // if ( in ) return;
        }
#endif
    }
    else
    {
        // projection on a vertex:
#if ( DIM == 2 )
        meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
        meca.mC(inx,   inx  ) -= stiff;
        meca.mC(inx+1, inx+1) -= stiff;
#endif
        meca.base(inx  )  += stiff * pX;
        meca.base(inx+1)  += stiff * pY;
    }
#endif
}

/// CASE 2: fiber end can slide on cell wall where it is initially found (changing wall is NOT allowed)
void SpacePolyRegWPlectin2::setInteractionFibEnds(Vector const& pos, Vector const& pos0, Mecapoint const& pe, Meca & meca, real stiff) const
{
#if ( DIM > 1 )
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    // define polygon object in the current configuration:
    Polygon poly_curr_;
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_curr_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_curr[p][0]   = std::sqrt(lambdaXY_curr) * radius0_ * cos( p * sector_angle_ref );
        x_curr[p][1]   = std::sqrt(lambdaXY_curr) * radius0_ * sin( p * sector_angle_ref );
        poly_curr_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_curr_.complete(REAL_EPSILON);

    real surface_curr_ = poly_curr_.surface();
    if ( surface_curr_ < 0 )
        poly_curr_.flip();

    assert_true( surface_curr_ > 0 );
    
    if ( poly_curr_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

    index_t inx = DIM * pe.matIndex();

    int hit;
    real pX, pY;
    int edg =  poly_curr_.project(pos0.XX, pos0.YY, pX, pY, hit);
    real nX = -poly_curr_.pts_[hit].dy;
    real nY =  poly_curr_.pts_[hit].dx;

    if ( edg )
    {
        // projection on an edge of normal (nX, nY) already normalized
        const real pr = ( pX * nX + pY * nY ) * stiff;

        meca.mC(inx  , inx  ) -= nX * nX * stiff;
        meca.mC(inx  , inx+1) -= nX * nY * stiff;
        meca.mC(inx+1, inx+1) -= nY * nY * stiff;

        meca.base(inx  )  += nX * pr;
        meca.base(inx+1)  += nY * pr;
#if ( DIM > 2 )
        if ( fabs(pos.ZZ) >= height0_ * lambdaZ_curr )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
            // if ( in ) return;
        }
#endif
    }
    else
    {
        // projection on a vertex:
#if ( DIM == 2 )
        meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
        meca.mC(inx,   inx  ) -= stiff;
        meca.mC(inx+1, inx+1) -= stiff;
#endif
        meca.base(inx  )  += stiff * pX;
        meca.base(inx+1)  += stiff * pY;
    }
#endif
}

/// CASE 3: fiber end is fixed at its initial location (no movement possible)
void SpacePolyRegWPlectin2::setInteractionFibEndsFixed(Vector const& pos, Vector const& pos0, Mecapoint const& pe, Meca & meca, real stiff) const
{
#if ( DIM > 1 )
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    // define polygon object in the current configuration:
    Polygon poly_curr_;
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_curr_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_curr[p][0]   = std::sqrt(lambdaXY_curr) * radius0_ * cos( p * sector_angle_ref );
        x_curr[p][1]   = std::sqrt(lambdaXY_curr) * radius0_ * sin( p * sector_angle_ref );
        poly_curr_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_curr_.complete(REAL_EPSILON);

    real surface_curr_ = poly_curr_.surface();
    if ( surface_curr_ < 0 )
        poly_curr_.flip();

    assert_true( surface_curr_ > 0 );
    
    if ( poly_curr_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

   index_t inx = DIM * pe.matIndex();

   int hit;
   real pX, pY;
   int edg =  poly_curr_.project(pos0.XX, pos0.YY, pX, pY, hit);
   real nX = -poly_curr_.pts_[hit].dy;
   real nY =  poly_curr_.pts_[hit].dx;
#endif
#if ( DIM == 2 )
   meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
   meca.mC(inx,   inx  ) -= stiff;
   meca.mC(inx+1, inx+1) -= stiff;
#endif
   meca.base(inx  )  += stiff * ( pos0.XX * std::sqrt(lambdaXY_curr) );
   meca.base(inx+1)  += stiff * ( pos0.YY * std::sqrt(lambdaXY_curr) );
#if ( DIM > 2 )
    if ( fabs(pos.ZZ) >= height0_ * lambdaZ_curr )
    {
        meca.mC(inx+2, inx+2) -= stiff;
        meca.base(inx+2)      += stiff * std::copysign(height0_ * lambdaZ_curr, pos.ZZ);
    }
#endif
}

//------------------------------------------------------------------------------

void SpacePolyRegWPlectin2::write(Outputter& out) const
{
    out.put_characters("polyregWplectin2", 16);
    out.writeUInt16(8);
    out.writeFloat(nsides_);
    out.writeFloat(radius0_);
    out.writeFloat(height0_);
    out.writeFloat(t_equil_);
    out.writeFloat(eps_rate_);
    out.writeFloat(hole_rad_);
    out.writeFloat(hole_pos_[0]);
    out.writeFloat(hole_pos_[1]);
}


void SpacePolyRegWPlectin2::setLengths(const real len[])
{
    nsides_      = len[0];
    radius0_     = len[1];
    height0_     = len[2];
    t_equil_     = len[3];
    eps_rate_    = len[4];
    hole_rad_    = len[5];
    hole_pos_[0] = len[6];
    hole_pos_[1] = len[7];
    update();
}

void SpacePolyRegWPlectin2::read(Inputter& in, Simul&, ObjectTag)
{
    real len[8] = { 0 };
    read_data(in, len, "polyregWplectin2");
    setLengths(len);
}

//------------------------------------------------------------------------------
//                         OPENGL  DISPLAY
//------------------------------------------------------------------------------

#ifdef DISPLAY
#include "opengl.h"
#include "gle.h"

bool SpacePolyRegWPlectin2::draw() const
{
#if ( DIM > 2 )
    // quantify current stretch based on rate and time
    real lambdaXY_curr = eps_rate_ * ( simul().time() - t_equil_ ) + 1.0;
    if ( lambdaXY_curr < 1.0 )
        lambdaXY_curr  = 1.0;
    real lambdaZ_curr  = 1.0;

    ////get the regular polygon by sampling a circle at nsides_ points with regular angular spacing
    const size_t fin = nsides_;
    GLfloat c[fin+1], s[fin+1];
    gle::circle(fin, c, s, GLfloat(radius0_));

    Vector2 x_ref[fin], x_curr[fin];

    GLfloat L = GLfloat(height0_ * lambdaZ_curr);

    glLineWidth(3);
    // display bottom
    glBegin(GL_LINE_LOOP);
    for ( size_t n = 0; n <= fin; ++n )
    {
        x_ref[n][0]  = c[n];
        x_ref[n][1]  = s[n];
        x_curr[n][0] = std::sqrt(lambdaXY_curr) * x_ref[n][0];
        x_curr[n][1] = std::sqrt(lambdaXY_curr) * x_ref[n][1];
        gle::gleVertex(x_curr[n][0], x_curr[n][1], -height0_ * lambdaZ_curr);
    }
    glEnd();
    
    // display bottom
    glBegin(GL_LINE_LOOP);
    for ( size_t n = 0; n <= fin; ++n )
    {
        gle::gleVertex(x_curr[n][0], x_curr[n][1], height0_ * lambdaZ_curr);
    }
    glEnd();

    // display sides
    glBegin(GL_TRIANGLE_STRIP);
    for ( size_t n = 0; n <= fin; ++n )
    {
        gle::gleVertex(x_curr[n][0], x_curr[n][1],  height0_ * lambdaZ_curr);
        gle::gleVertex(x_curr[n][0], x_curr[n][1], -height0_ * lambdaZ_curr);
    }
    if ( 0 < fin )
    {
        gle::gleVertex(x_curr[0][0], x_curr[0][1],  height0_ * lambdaZ_curr);
        gle::gleVertex(x_curr[0][0], x_curr[0][1], -height0_ * lambdaZ_curr);
    }
    glEnd();

    ////draw the cylindrical hole if defined
    if ( hole_rad_ > 0 )
    {
        const size_t fin = 100;
        GLfloat c[fin+1], s[fin+1];
        gle::circle(fin, c, s, GLfloat(hole_rad_));

        Vector2 x_ref[fin], x_curr[fin];

        GLfloat L = GLfloat(height0_ * lambdaZ_curr);

        glLineWidth(3);
        // display bottom
        glBegin(GL_LINE_LOOP);
        for ( size_t n = 0; n <= fin; ++n )
        {
            x_ref[n][0]  = hole_pos_[0] + c[n];
            x_ref[n][1]  = hole_pos_[1] + s[n];
            gle::gleVertex(x_ref[n][0], x_ref[n][1], -height0_ * lambdaZ_curr);
        }
        glEnd();
        
        // display top
        glBegin(GL_LINE_LOOP);
        for ( size_t n = 0; n <= fin; ++n )
        {
            gle::gleVertex(x_ref[n][0], x_ref[n][1], height0_ * lambdaZ_curr);
        }
        glEnd();

        // display sides
        glBegin(GL_TRIANGLE_STRIP);
        for ( size_t n = 0; n <= fin; ++n )
        {
            gle::gleVertex(x_ref[n][0], x_ref[n][1],  height0_ * lambdaZ_curr);
            gle::gleVertex(x_ref[n][0], x_ref[n][1], -height0_ * lambdaZ_curr);
        }
        if ( 0 < fin )
        {
            gle::gleVertex(x_ref[0][0], x_ref[0][1],  height0_ * lambdaZ_curr);
            gle::gleVertex(x_ref[0][0], x_ref[0][1], -height0_ * lambdaZ_curr);
        }
        glEnd();
    }

#endif
    return true;
}

#else

bool SpacePolyRegWPlectin2::draw() const
{
    return false;
}

#endif
