// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// SpacePolyRegNoTopCut class implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-23)
/*
   This class is based on SpaceRing and SpacePolygonNoTop. It is used to model a right regular
   prism by prescribing its height, the number of sides and the radius of the circle that
   inscribes the prism base, as well as the current deformation from the reference.
*/

#include "dim.h"
#include "space_polyregNoTopCut.h"
#include "exceptions.h"
#include "mecapoint.h"
#include "iowrapper.h"
#include "glossary.h"
#include "polygon.h"
#include "meca.h"
#include "fiber.h"
#include "fiber_segment.h"
#include "fiber_prop.h"
#include "simul.h"
#include <fstream>

SpacePolyRegNoTopCut::SpacePolyRegNoTopCut(SpaceProp const* p)
: Space(p)
{
    if ( DIM < 3 )
        throw InvalidParameter("polyreg is currently only implemented in 3D");
    height0_ = 1;
    radius_ = 1;
    nsides_ = 6;
    cut_ = 1.0;
    Fvals_[0] = 1;
    Fvals_[1] = 0;
    Fvals_[2] = 0;
    Fvals_[3] = 1;
    Fvals_[4] = 1;
}

SpacePolyRegNoTopCut::~SpacePolyRegNoTopCut()
{
}

void SpacePolyRegNoTopCut::resize(Glossary& opt)
{
    real rad = radius_, cut = cut_, Fval[5];

    if ( opt.set(rad, "diameter") )
        rad *= 0.5;
    else opt.set(rad, "radius");
    opt.set(cut, "cut_height");
    for ( int d = 0; d < 5; ++d )
    {
        real Fval = Fvals_[d];
        opt.set(Fval, "deforgrad", d);
        if ( Fval < 0 )
            throw InvalidParameter("polyregNoTopCut:def gradient entries must be >= 0");
        Fvals_[d] = Fval;
    }

    if ( rad < 0 )
        throw InvalidParameter("polyregNoTopCut:radius must be >= 0");
    if ( cut < 0 || cut > 1 )
        throw InvalidParameter("polyregNoTopCut:number of sides must be within [0;1]");

    radius_ = rad;
    cut_    = cut;

#if ( DIM == 3 )
    real len = height0_;
    if ( opt.set(len, "height") )
        len *= 0.5;
    if ( len < 0 )
        throw InvalidParameter("polygon:height must be >= 0");
    height0_ = len;
#endif

    update();
}

void SpacePolyRegNoTopCut::update()
{
    // define polygon object:
    unsigned nbp = nsides_;

    real sector_angle_ref = 2.0*M_PI/nbp;
    Vector2 x_ref[nbp+1], x_curr[nbp+1];
    real edge_angles[nbp+1];

    poly_.allocate(nbp);
    for ( unsigned p = 0; p < nbp; ++p )
    {
        x_ref[p][0]    = radius_ * cos( p * sector_angle_ref );
        x_ref[p][1]    = radius_ * sin( p * sector_angle_ref );

        x_curr[p][0]   = Fvals_[0] * x_ref[p][0] + Fvals_[1] * x_ref[p][1];
        x_curr[p][1]   = Fvals_[2] * x_ref[p][0] + Fvals_[3] * x_ref[p][1];
        poly_.setPoint(p, x_curr[p][0], x_curr[p][1]);
    }
    poly_.complete(REAL_EPSILON);

    surface_ = poly_.surface();
    if ( surface_ < 0 )
        poly_.flip();

    assert_true( surface_ > 0 );
    
    if ( poly_.complete(REAL_EPSILON) )
        throw InvalidParameter("unfit polygon: consecutive points may overlap");

}

bool SpacePolyRegNoTopCut::inside(Vector const& w) const
{
#if ( DIM > 2 )
   if ( w.ZZ < -height0_*Fvals_[4] )
       return false;
#endif
#if ( DIM > 1 )
    return poly_.inside(w.XX, w.YY, 1);
#else
    return false;
#endif
}

Vector SpacePolyRegNoTopCut::randomPlace() const
{
    if ( surface_ <= 0 )
        throw InvalidParameter("cannot pick point inside polygon of null surface");
    return Space::randomPlace();
}

//------------------------------------------------------------------------------
/**
 Check if point (w.XX, w.YY, w.ZZ) is inside polygon
 */
Vector SpacePolyRegNoTopCut::project(Vector const& w) const
{
    Vector p;
#if ( DIM == 1 )
    
    p.XX = w.XX;
    
#elif ( DIM == 2 )
    
    int hit;
    poly_.project(w.XX, w.YY, p.XX, p.YY, hit);
    
#elif ( DIM > 2 )
    
    if ( w.ZZ < -height0_*Fvals_[4] )
    {
        if ( poly_.inside(w.XX, w.YY, 1) )
        {
            // too low in the Z axis, but inside XY
            p.XX = w.XX;
            p.YY = w.YY;
        }
        else
        {
            // outside in Z and XY
            int hit;
            poly_.project(w.XX, w.YY, p.XX, p.YY, hit);
        }
        p.ZZ = -height0_*Fvals_[4];
    }
    else
    {
        int hit;
        poly_.project(w.XX, w.YY, p.XX, p.YY, hit);
        if ( poly_.inside(w.XX, w.YY, 1) )
        {
            // inside in the Z axis and the XY polygon:
            // to the polygonal edge in XY plane:
            real hh = (w.XX-p.XX)*(w.XX-p.XX) + (w.YY-p.YY)*(w.YY-p.YY);
            // to the bottom plates:
            real v = -height0_*Fvals_[4] + w.ZZ;
            // compare distances
            if ( v * v < hh )
                return Vector(w.XX, w.YY, -height0_*Fvals_[4]);
        }
        p.ZZ = w.ZZ;
    }
    
#endif
    return p;
}

//------------------------------------------------------------------------------

/// interactions to be applied to all meca points
void SpacePolyRegNoTopCut::setInteraction(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff) const
{
#if ( DIM > 1 )
    index_t inx = DIM * pe.matIndex();
    
    int hit;
    real pX, pY;
    int edg = poly_.project(pos.XX, pos.YY, pX, pY, hit);
    real nX = -poly_.pts_[hit].dy;
    real nY =  poly_.pts_[hit].dx;
    
#if ( DIM > 2 )
    bool in = poly_.inside(pos.XX, pos.YY, 1);

    if ( pos.ZZ <= -height0_*Fvals_[4] )
    {
        meca.mC(inx+2, inx+2) -= stiff;
        meca.base(inx+2)      -= stiff * height0_*Fvals_[4];
        if ( in ) return;
    }
    else if ( in )
    {
        // Compare distance to bottom plate:
        real v = -height0_*Fvals_[4] + pos.ZZ;
        // and distance to polygonal edge in XY plane:
        real hh = (pos.XX-pX)*(pos.XX-pX) + (pos.YY-pY)*(pos.YY-pY);
        
        if ( v * v < hh )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      -= stiff * height0_*Fvals_[4];
            return;
        }
    }
#endif

    if ( edg )
    {
        // projection on an edge of normal (nX, nY) already normalized
        const real pr = ( pX * nX + pY * nY ) * stiff;
        
        meca.mC(inx  , inx  ) -= nX * nX * stiff;
        meca.mC(inx  , inx+1) -= nX * nY * stiff;
        meca.mC(inx+1, inx+1) -= nY * nY * stiff;
        
        meca.base(inx  )  += nX * pr;
        meca.base(inx+1)  += nY * pr;
    }
    else
    {
        // projection on a vertex:
#if ( DIM == 2 )
        meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
        meca.mC(inx,   inx  ) -= stiff;
        meca.mC(inx+1, inx+1) -= stiff;
#endif
        meca.base(inx  )  += stiff * pX;
        meca.base(inx+1)  += stiff * pY;
    }
#endif
}

/// interactions to be applied to any point falling within a distance `inter_rad` from cell walls, only for the bottom half of the prism height
void SpacePolyRegNoTopCut::setInteractionLinIns(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff, real inter_rad) const
{
#if ( DIM > 1 )
    index_t inx = DIM * pe.matIndex();
    
    int hit;
    real pX, pY;
    int edg = poly_.project(pos.XX, pos.YY, pX, pY, hit);
    real dX = pos.XX - pX;
    real dY = pos.YY - pY;
    real nX = -poly_.pts_[hit].dy;
    real nY =  poly_.pts_[hit].dx;
    real dNdN = dX * nX + dY * nY;

    if ( edg && (dNdN < inter_rad * inter_rad) )
    {
        // projection on an edge of normal (nX, nY) already normalized
        const real pr = ( pX * nX + pY * nY ) * stiff;
        
        meca.mC(inx  , inx  ) -= nX * nX * stiff;
        meca.mC(inx  , inx+1) -= nX * nY * stiff;
        meca.mC(inx+1, inx+1) -= nY * nY * stiff;
        
        meca.base(inx  )  += nX * pr;
        meca.base(inx+1)  += nY * pr;
#if ( DIM > 2 )
        if ( -pos.ZZ >= height0_*Fvals_[4] )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      -= stiff * height0_*Fvals_[4];
            // if ( in ) return;
        }
        else if ( pos.ZZ > -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4] )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      -= stiff * 0;
            // if ( in ) return;
        }
    }
    else
    {
        // projection on a vertex:
        if ( (dNdN < inter_rad * inter_rad) && (pos.ZZ < -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4]) )
        {
            // projection on a vertex:
            meca.mC(inx,   inx  ) -= stiff;
            meca.mC(inx+1, inx+1) -= stiff;
            meca.base(inx  )  += stiff * pX;
            meca.base(inx+1)  += stiff * pY;
        }
#endif
    }

#if ( DIM > 2 )
    if ( pos.ZZ < -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4] )
    {
        real dZ = pos.ZZ + height0_*Fvals_[4];
        if ( dZ * dZ < inter_rad * inter_rad )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      -= stiff * height0_*Fvals_[4];
        }
    }
#endif

    Mecable * nucleus  = simul().findMecable("cell_nucleus");
    if ( nucleus )
    {
        real dragN   = nucleus->dragCoefficient();
        Vector posN  = nucleus->posP(0);
        real radN    = dragN / (6 * M_PI * simul().viscosity());
        Vector pN    = posN + radN * (pos - posN) / (pos - posN).norm();

        if ( (pos - pN).normSqr() < inter_rad * inter_rad )
            meca.addLongLink(pe, Mecapoint(nucleus, 0), radN, stiff);
    }
#endif
}

/// interactions to be applied only to fiber ends, we consider 3 cases:
/// 1) each fiber end point is to be maintained on the closes side wall of the enclosing cell, but can slide on it
/// 2) each fiber end point is to be maintained on the side wall where it is initially found, but can slide on it
/// 3) each fiber end point is to be maintained fixed at its initial position

/// CASE 1: fiber end can slide on closest cell wall (changing wall is allowed)
void SpacePolyRegNoTopCut::setInteractionFibEnds(Vector const& pos, Mecapoint const& pe, Meca & meca, real stiff) const
{
#if ( DIM > 1 )
    index_t inx = DIM * pe.matIndex();

    int hit;
    real pX, pY;
    int edg = poly_.project(pos.XX, pos.YY, pX, pY, hit);
    real nX = -poly_.pts_[hit].dy;
    real nY =  poly_.pts_[hit].dx;

    if ( edg )
    {
        // projection on an edge of normal (nX, nY) already normalized
        const real pr = ( pX * nX + pY * nY ) * stiff;

        meca.mC(inx  , inx  ) -= nX * nX * stiff;
        meca.mC(inx  , inx+1) -= nX * nY * stiff;
        meca.mC(inx+1, inx+1) -= nY * nY * stiff;

        meca.base(inx  )  += nX * pr;
        meca.base(inx+1)  += nY * pr;
#if ( DIM > 2 )
        if ( pos.ZZ <= -height0_*Fvals_[4] )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      -= stiff * height0_ * Fvals_[4];
            // if ( in ) return;
        }
        if ( pos.ZZ >= -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4] )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      += stiff * ( - height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4]);
        }
#endif
    }
    else
    {
        // projection on a vertex:
#if ( DIM == 2 )
        meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
        meca.mC(inx,   inx  ) -= stiff;
        meca.mC(inx+1, inx+1) -= stiff;
#endif
        meca.base(inx  )  += stiff * pX;
        meca.base(inx+1)  += stiff * pY;
    }
#endif
}

/// CASE 2: fiber end can slide on cell wall where it is initially found (changing wall is NOT allowed)
void SpacePolyRegNoTopCut::setInteractionFibEnds(Vector const& pos, Vector const& pos0, Mecapoint const& pe, Meca & meca, real stiff) const
{
#if ( DIM > 1 )
    index_t inx = DIM * pe.matIndex();

    int hit;
    real pX, pY;
    int edg = poly_.project(pos0.XX, pos0.YY, pX, pY, hit);
    real nX = -poly_.pts_[hit].dy;
    real nY =  poly_.pts_[hit].dx;

    if ( edg )
    {
        // projection on an edge of normal (nX, nY) already normalized
        const real pr = ( pX * nX + pY * nY ) * stiff;

        meca.mC(inx  , inx  ) -= nX * nX * stiff;
        meca.mC(inx  , inx+1) -= nX * nY * stiff;
        meca.mC(inx+1, inx+1) -= nY * nY * stiff;

        meca.base(inx  )  += nX * pr;
        meca.base(inx+1)  += nY * pr;
#if ( DIM > 2 )
        if ( pos.ZZ <= -height0_*Fvals_[4] )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      -= stiff * height0_ * Fvals_[4];
            // if ( in ) return;
        }
        if ( pos.ZZ >= -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4] )
        {
            meca.mC(inx+2, inx+2) -= stiff;
            meca.base(inx+2)      += stiff * ( - height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4]);
        }
#endif
    }
    else
    {
        // projection on a vertex:
#if ( DIM == 2 )
        meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
        meca.mC(inx,   inx  ) -= stiff;
        meca.mC(inx+1, inx+1) -= stiff;
#endif
        meca.base(inx  )  += stiff * pX;
        meca.base(inx+1)  += stiff * pY;
    }
#endif
}

/// CASE 3: fiber end is fixed at its initial location (no movement possible)
void SpacePolyRegNoTopCut::setInteractionFibEndsFixed(Vector const& pos, Vector const& pos0, Mecapoint const& pe, Meca & meca, real stiff) const
{
#if ( DIM > 1 )
   index_t inx = DIM * pe.matIndex();

   int hit;
   real pX, pY;
   int edg = poly_.project(pos0.XX, pos0.YY, pX, pY, hit);
   real nX = -poly_.pts_[hit].dy;
   real nY =  poly_.pts_[hit].dx;
#endif
#if ( DIM == 2 )
   meca.mB(pe.matIndex(), pe.matIndex()) -= stiff;
#elif ( DIM > 2 )
   meca.mC(inx,   inx  ) -= stiff;
   meca.mC(inx+1, inx+1) -= stiff;
#endif
   meca.base(inx  )  += stiff * ( pos0.XX * Fvals_[0] + pos0.YY * Fvals_[1] );
   meca.base(inx+1)  += stiff * ( pos0.XX * Fvals_[2] + pos0.YY * Fvals_[3] );
#if ( DIM > 2 )
   if ( pos.ZZ <= -height0_*Fvals_[4] )
   {
       meca.mC(inx+2, inx+2) -= stiff;
       meca.base(inx+2)      -= stiff * height0_ * Fvals_[4];
   }
    if ( pos.ZZ >= -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4] )
   {
       meca.mC(inx+2, inx+2) -= stiff;
       meca.base(inx+2)      += stiff * ( - height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4]);
   }
#endif
}

//------------------------------------------------------------------------------

void SpacePolyRegNoTopCut::write(Outputter& out) const
{
    out.put_characters("polyregNoTopCut", 16);
    out.writeUInt16(8);
    out.writeFloat(height0_);
    out.writeFloat(radius_);
    out.writeFloat(cut_);
    out.writeFloat(Fvals_[0]);
    out.writeFloat(Fvals_[1]);
    out.writeFloat(Fvals_[2]);
    out.writeFloat(Fvals_[3]);
    out.writeFloat(Fvals_[4]);
}


void SpacePolyRegNoTopCut::setLengths(const real len[])
{
    height0_  = len[0];
    radius_   = len[1];
    cut_      = len[2];
    Fvals_[0] = len[3];
    Fvals_[1] = len[4];
    Fvals_[2] = len[5];
    Fvals_[3] = len[6];
    Fvals_[4] = len[7];
    update();
}

void SpacePolyRegNoTopCut::read(Inputter& in, Simul&, ObjectTag)
{
    real len[8] = { 0 };
    read_data(in, len, "polyregNoTopCut");
    setLengths(len);
}

//------------------------------------------------------------------------------
//                         OPENGL  DISPLAY
//------------------------------------------------------------------------------

#ifdef DISPLAY
#include "opengl.h"
#include "gle.h"

bool SpacePolyRegNoTopCut::draw() const
{
#if ( DIM > 2 )

    ////get the regular polygon by sampling a circle at nsides_ points with regular angular spacing
    const size_t fin = nsides_;
    GLfloat c[fin+1], s[fin+1];
    gle::circle(fin, c, s, GLfloat(radius_));

    Vector2 x_ref[fin], x_curr[fin];

    GLfloat L = GLfloat(Fvals_[4] * height0_);

    glLineWidth(3);
    // display bottom
    glBegin(GL_LINE_LOOP);
    for ( size_t n = 0; n <= fin; ++n )
    {
        x_ref[n][0]  = c[n];
        x_ref[n][1]  = s[n];
        x_curr[n][0] = Fvals_[0] * x_ref[n][0] + Fvals_[1] * x_ref[n][1];
        x_curr[n][1] = Fvals_[2] * x_ref[n][0] + Fvals_[3] * x_ref[n][1];
        gle::gleVertex(x_curr[n][0], x_curr[n][1], -height0_*Fvals_[4]);
    }
    glEnd();
    
    // display bottom
    glBegin(GL_LINE_LOOP);
    for ( size_t n = 0; n <= fin; ++n )
    {
        gle::gleVertex(x_curr[n][0], x_curr[n][1], -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4]);
    }
    glEnd();

    // display sides
    glBegin(GL_TRIANGLE_STRIP);
    for ( size_t n = 0; n <= fin; ++n )
    {
        gle::gleVertex(x_curr[n][0], x_curr[n][1], -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4]);
        gle::gleVertex(x_curr[n][0], x_curr[n][1], -height0_*Fvals_[4]);
    }
    if ( 0 < fin )
    {
        gle::gleVertex(x_curr[0][0], x_curr[0][1], -height0_*Fvals_[4] + 2*cut_*height0_*Fvals_[4]);
        gle::gleVertex(x_curr[0][0], x_curr[0][1], -height0_*Fvals_[4]);
    }
    glEnd();

#endif
    return true;
}

#else

bool SpacePolyRegNoTopCut::draw() const
{
    return false;
}

#endif
