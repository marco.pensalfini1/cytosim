// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// SpacePolyReg class implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This class is based on SpaceRing and SpacePolygon. It is used to model a right regular
   prism by prescribing its height, the number of sides and the radius of the circle that
   inscribes the prism base, as well as the current deformation from the reference.
*/

#ifndef SPACE_POLYREG_NOTOPHALF2_H
#define SPACE_POLYREG_NOTOPHALF2_H

#include "space.h"
#include "polygon.h"

/// a right regular prism of axis Z, but without caps
/**
 Space `polyregNoTopHalf2` is the extrusion of a regular polygon along the Z-axis to reach a height `height`.
 The cross section in the XY plane is a regular polygon of `nsides` sides.
 The bottom polygon is part of the surface but the top polygon is not.
 project() will always project:
 i) to the surface of the prism wall that corresponds to the sector where the mecapoint is found,
    the sectors are obtained by connecting the center of the polygonal base with the nsides vertices
 ii) to the bottom prism face, if the meca point is below the prism height along Z

 Parameters:
     - nsides  = number of equal length sides for the polygonal base
     - radius  = radius of the inscribing circle used to construct the polygonal base (also in the reference)
     - deforgrad = deformation gradient tensor, values to be entered as F11, F12, F21, F22, F33 (Fi3 = 0 if i != 3)
     - height  = total extent of the cylinder in Z in reference configuration

 @ingroup SpaceGroup
 */
class SpacePolyRegNoTopHalf2 : public Space
{
    /// apply a force directed towards the edge of the Space
    static void setInteraction(Vector const& pos, Mecapoint const&, Meca &, real stiff, const real len, const real apo, const real nsid, const real Fval[] );

private:
    
    /// The regular polygon object used to obtain the prism
    Polygon     poly_;

    /// half the reference height of the prism
    real  height0_;
    
    /// the radius of the inscribing circle used to build the prism base
    real  radius_;

    /// the number of sides of the prism polygonal base
    real  nsides_;

    /// the deformation gradient tensor (2x2 plus F33)
    real  Fvals_[5];

    /// Surface of polygon
    real  surface_;

    /// calculate apothem_ and side_
    void  update();

public:
        
    ///constructor
    SpacePolyRegNoTopHalf2(const SpaceProp *);
    
    /// destructor
    ~SpacePolyRegNoTopHalf2();

    /// change dimensions
    void        resize(Glossary& opt);

    /// the volume inside
    real        volume() const { return ( DIM>2 ? 2*(height0_*Fvals_[4]) : 1 ) * surface_; }

    /// number of sides in the polygon
    int         nsides() const { return nsides_;}

    /// current prism height
    real        height() const { return 2*height0_*Fvals_[4];}

    /// radius of the polygon's inscribing circle
    real        radius() const { return radius_;}

    /// current deformation gradient in the plane
    Vector4     deforgrad2D() const { return Vector4(Fvals_[0],Fvals_[1],Fvals_[2],Fvals_[3]);}
    
    /// true if the point is inside the Space
    bool        inside(Vector const&) const;

    /// a random position inside the volume
    Vector      randomPlace() const;

    /// set `proj` as the point on the edge that is closest to `point`
    Vector      project(Vector const& pos) const;

    /// apply a force directed towards the edge of the Space for any meca point
    void        setInteraction(Vector const& pos, Mecapoint const&, Meca &, real stiff) const;

    /// apply a force directed towards the edge of the Space for any point within a distance `inter_rad` from cell walls
    void        setInteractionLinIns(Vector const& pos, Mecapoint const&, Meca &, real stiff, real inter_rad) const;

    /// apply a force directed towards the edge of the Space to ensure that fiber ends slide on closest cell wall
    void        setInteractionFibEnds(Vector const& pos, Mecapoint const&, Meca &, real stiff) const;

    /// apply a force directed towards the edge of the Space to constrain fiber ends on the cell wall where they were initially
    void        setInteractionFibEnds(Vector const& pos, Vector const& pos0, Mecapoint const&, Meca &, real stiff) const;

    /// apply a force directed towards the initial position (pos0) of a fiber end located at `pos`
    void        setInteractionFibEndsFixed(Vector const& pos, Vector const& pos0, Mecapoint const&, Meca &, real stiff) const;
    
    /// OpenGL display function; returns true if successful
    bool        draw() const;
    
    /// write to file
    void        write(Outputter&) const;

    /// get dimensions from array `len`
    void        setLengths(const real len[8]);
    
    /// read from file
    void        read(Inputter&, Simul&, ObjectTag);

};

#endif

