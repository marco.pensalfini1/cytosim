// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// StretchyFiber class implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This is based on GrowingFiber and leverages the force applied at the fiber ends to
   drive elongation according to a prescribed constitutive behavior.
*/

#ifndef STRETCHY_FIBER_PROP
#define STRETCHY_FIBER_PROP

#include "fiber_prop.h"

class StretchyFiberProp : public FiberProp
{
    
    friend class StretchyFiber;
    
public:
    
    /**
     @defgroup StretchyFiberPar Parameters of StretchyFiber
     @ingroup Parameters
     Inherits @ref FiberPar.
     @{
     */
    
    /// see @ref StretchyFiber
    
    /// Fiber parameters correspond to two Kelvin-Voigt models in series
    ///     0: modulus of 1st spring times fiber cross-sectional area (E1*Af)
    ///     1: retardation time of the 1st Kelvin-Voigt model (Tau1 = eta1/E1)
    ///     2: modulus of 2nd spring times fiber cross-sectional area (E2*Af)
    ///     3: retardation time of the 2nd Kelvin-Voigt model (Tau2 = eta2/E2)
    real    constitutive[4];
    
    /// Additional fiber parameters allowing to model nonlinear-elastic response
    ///     0: modulus of 1st spring times fiber cross-sectional area in softening regime (E1*Af)_soften
    ///     1: modulus of 1st spring times fiber cross-sectional area in restiffening  regime (E1*Af)_stiffen
    ///     2: value of fiber strain inducing softening (eps_soften)
    ///     3: value of fiber strain inducing restiffening (eps_stiffen)
    ///     4: scalar prescribing the smoothness of the transition between values of E1*Af (gamma)
    real    nonlinear[5];

public:
    
    /// constructor
    StretchyFiberProp(const std::string& n) : FiberProp(n) { clear(); }

    /// destructor
    ~StretchyFiberProp() { }
    
    /// return a Fiber with this property
    Fiber* newFiber() const;
    
    /// set default values
    void clear();
       
    /// set using a Glossary
    void read(Glossary&);
   
    /// check and derive parameter values
    void complete(Simul const&);
    
    /// return a carbon copy of object
    Property* clone() const { return new StretchyFiberProp(*this); }

    /// write
    void write_values(std::ostream&) const;

};

#endif

