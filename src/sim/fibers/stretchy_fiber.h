// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// StretchyFiber class implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This is based on GrowingFiber and leverages the force applied at the fiber ends to
   drive elongation according to a prescribed constitutive behavior.
*/

#ifndef STRETCHY_FIBER_H
#define STRETCHY_FIBER_H

#include "sim.h"
#include "vector.h"
#include "node_list.h"
#include "fiber.h"

class StretchyFiberProp;


/// A Fiber that elongates according to a prescribed constitutive behavior in response to an applied force.
/**
 The general form of the fiber constitutive behavior is that of 2 Kelvin-Voigt viscoelastic models in series.
 
 The constitutive parameters are passed using the keyword `constitutive` in the following order:
 
 * constitutive[0] = E1*Af, the product of the fiber cross-sectional area with the elastic modulus of the 1st spring
 * constitutive[1] = Tau1 = eta1/E1, the retardation time of the 1st Kelvin-Voigt model
 * constitutive[2] = E2*Af, the product of the fiber cross-sectional area with the elastic modulus of the 2nd spring
 * constitutive[3] = Tau2 = eta2/E2, the retardation time of the 2nd Kelvin-Voigt model

 To model a nonlinear-elastic fiber, one can additionally pass the keyword `nonlinear` with the following parameters:

 * nonlinear[0] = (E1*Af)_soften, value of E1*Af during the fiber softening that happens between eps_soften and eps_stiffen
 * nonlinear[1] = (E1*Af)_stiffen, value of E1*Af during the fiber restiffening that happens past eps_stiffen
 * nonlinear[2] = eps_soften, strain value past which the fiber softens to reach (E1*Af)_soften
 * nonlinear[3] = eps_stiffen, strain value past which the fiber restiffens to reach (E1*Af)_stiffen
 * nonlinear[4] = gamma, scalar prescribing the smoothness of the transition between values of E1*Af
 
 EXAMPLE 1:
 To model a linear-elastic fiber, it is sufficient to define E1*Af and set a value of Tau1 that is small enough to avoid
 excessively influencing the slope of the force-strain relationship, but large enough to avoid the `resonant` behavior
 that would result from setting Tau1 = 0. Finding the right value of Tau1 might require some trial and error.

 EXAMPLE 2:
 To model the rate-dependent behavior that is typical of many biopolymers, one can additionally define E2*Af and Tau2.
 * Tau2 should be comparable to the duration of fiber loading, so that the dashpot can delay fiber shortening in unloading
 * extremely slow loading results in the two springs of moduli E1 and E2 being in series
    -> the quasi-static response is linear with modulus E1*E2/(E1+E2)
 * extremely fast loading results in the spring of modulus E2 being blocked by its parallel dashpot
    -> the instantaneous response is linear with modulus E1

 More complicated constitutive relationships can be implemented by modifying the creep compliance function, which is based
 on the superposition principle.

 Note: this class does not support simulation restart since fiber elongation is based on its loading history

 In line with GrowingFiber, the length will not exceed `fiber:max_length`, and any Fiber shorter than `fiber:min_length`
 will be deleted.

 See the @ref GrowingFiberPar.
 @ingroup FiberGroup
 */
class StretchyFiber : public Fiber
{
private:
    
    /// state of PLUS_END (static or growing)
    state_t    mStateP;
    
    /// assembly at PLUS_END during last time-step
    real       mGrowthP;
    
    /// state of MINUS_END (static or growing)
    state_t    mStateM;
    
    /// assembly at MINUS_END during last time-step
    real       mGrowthM;
    
public:
    
    /// the Property of this object
    StretchyFiberProp const* prop;
    
    /// constructor
    StretchyFiber(StretchyFiberProp const*);
    
    /// destructor
    virtual ~StretchyFiber();
    
    //--------------------------------------------------------------------------
    
    /// return assembly/disassembly state of MINUS_END
    state_t     dynamicStateM() const { return mStateM; };
    
    /// change state of MINUS_END
    void        setDynamicStateM(state_t s);
    
    /// length increment at MINUS_END during last time-step
    real        freshAssemblyM() const { return mGrowthM; }

    
    /// return assembly/disassembly state of PLUS_END
    state_t     dynamicStateP() const { return mStateP; };
    
    /// change state of PLUS_END
    void        setDynamicStateP(state_t s);

    /// length increment at PLUS_END during last time-step
    real        freshAssemblyP() const { return mGrowthP; }

    /// force value at PLUS_END at current time-step
    real        forceCurrentP;

    /// force value at PLUS_END at previous time-step
    real        forcePreviousP;

    /// array containing force increments at PLUS_END
    std::vector<real> deltaForceP;

    /// force value at MINUS_END at current time-step
    real        forceCurrentM;

    /// force value at MINUS_END at previous time-step
    real        forcePreviousM;

    /// array containing force increments at MINUS_END
    std::vector<real> deltaForceM;

    /// array containing values of E1*Af over time
    std::vector<real> E1Afvals;

    /// monte-carlo step
    void        step();
    
    //--------------------------------------------------------------------------
    
    /// write to Outputter
    void        write(Outputter&) const;
    
    /// read from Inputter
    void        read(Inputter&, Simul&, ObjectTag);
    
};


#endif
