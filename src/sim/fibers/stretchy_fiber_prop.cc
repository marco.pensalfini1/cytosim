// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// StretchyFiber class implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This is based on GrowingFiber and leverages the force applied at the fiber ends to
   drive elongation according to a prescribed constitutive behavior.
*/

#include <cmath>
#include "stretchy_fiber_prop.h"
#include "stretchy_fiber.h"
#include "property_list.h"
#include "exceptions.h"
#include "glossary.h"
#include "simul.h"


Fiber* StretchyFiberProp::newFiber() const
{
    return new StretchyFiber(this);
}


void StretchyFiberProp::clear()
{
    FiberProp::clear();
    for ( int i = 0; i < 4; ++i )
        constitutive[i]      = INFINITY;
    for ( int i = 0; i < 5; ++i )
        nonlinear[i]   = INFINITY;
}


void StretchyFiberProp::read(Glossary& glos)
{
    FiberProp::read(glos);
    glos.set(constitutive, 4, "constitutive");
    glos.set(nonlinear,    5, "nonlinear");
}


void StretchyFiberProp::complete(Simul const& sim)
{
    FiberProp::complete(sim);
    for ( int i = 0; i < 4; ++i )
    {
        if ( constitutive[i] < 0)
            throw InvalidParameter("fiber:constitutive should be >= 0");
    }
    for ( int i = 0; i < 5; ++i )
    {
        if ( nonlinear[i] < 0 )
            throw InvalidParameter("fiber:nonlinear parameters should be >= 0");
    }
}


void StretchyFiberProp::write_values(std::ostream& os) const
{
    FiberProp::write_values(os);
    write_value(os, "constitutive",   constitutive, 4);
    write_value(os, "nonlinear",      nonlinear, 5);
}
