// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// Nematic organizer was implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This organizer models a collection of fibers whose nematic director and order can be 
   controlled externally via a potential of prescribed stiffness. All fibers share the 
   same individual length and follow a simple turnover algorithm.
*/

#include "nematic_prop.h"
#include "property_list.h"
#include "glossary.h"
#include "simul.h"


void NematicProp::clear()
{
    stiffness    = 0;
    nematicS     = 0;
    length       = 1;
    direction[0] = 1;
    direction[1] = 0;
    fiber_rate   = 0;
}


void NematicProp::read(Glossary& glos)
{
    glos.set(stiffness,    "stiffness");
    glos.set(nematicS,     "nematicS");
    glos.set(length,       "length");
    glos.set(direction, 2, "direction");
    glos.set(fiber_rate,   "turnover_rate");
}


void NematicProp::complete(Simul const& sim)
{

    if ( length <= 0 )
        throw InvalidParameter("nematic:length must be specified and > 0");

    if ( stiffness < 0 )
        throw InvalidParameter("nematic:stiffness must be specified and >= 0");

    if ( nematicS < 0 || nematicS > 1 )
        throw InvalidParameter("nematic:parameter S must be between 0 and 1");

    if ( fiber_rate < 0 )
        throw InvalidParameter("nematic:turnover_rate must be >= 0");
    
}


void NematicProp::write_values(std::ostream& os) const
{
    write_value(os, "stiffness", stiffness);
    write_value(os, "nematicS", nematicS);
    write_value(os, "length",    length);
    write_value(os, "direction", direction,  2);
    write_value(os, "turnover_rate", fiber_rate);
}

