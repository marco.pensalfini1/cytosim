// Cytosim was created by Francois Nedelec. Copyright 2007-2017 EMBL.
// Nematic organizer was implemented by Marco Pensalfini while at UPC-BarcelonaTECH (2020-22)
/*
   This organizer models a collection of fibers whose nematic director and order can be 
   controlled externally via a potential of prescribed stiffness. All fibers share the 
   same individual length and follow a simple turnover algorithm.
*/

#ifndef NEMATIC_H
#define NEMATIC_H

#include "object.h"
#include "organizer.h"
#include "nematic_prop.h"
#include "fiber.h"

//------------------------------------------------------------------------------

///a set of Fiber forming a nematic set with a simplistic turnover
/**
 The Fibers have length 'prop->length', turn over at a rate 'prop->fiber_rate',
 and are disposed to provide a nematic set with director nemDir and order parameter nemS.
 - object(n) are Fiber.
 - The stiffness of the external potential is 'prop->stiffness'.
 .
 
 @ingroup OrganizerGroup
 */
class Nematic : public Organizer
{
    
private:

    /// Numer of fiber segments in the organizer
    unsigned mutable numSeg;

    /// Nematic order parameter S
    real mutable nemS;

    /// Nematic director
    Vector2 mutable nemDir;

#if ( DIM == 2 )
    /// Target Q matrix
    Matrix22 mutable Q_target;

    /// Current Q matrix
    Matrix22 mutable Q_curr;

    /// Generalized stress that is power conjugate of Q (nemStress = 0.5 * nemStiff * (Q_curr - Q_target) )
    Matrix22 mutable nemStress;
#endif

    /// Energy penalizing deviations of Q_curr from Q_target
    real mutable nemEnergy;

    /// Stiffness of energy penalizing deviations of Q_curr from Q_target
    real mutable nemStiff;

public:

    /// Property
    NematicProp const* prop;
    
    /// constructor
    Nematic(NematicProp const* p) : prop(p) {}
    
    /// destructor  
    virtual   ~Nematic();
    
    /// construct all the dependent Objects of the Organizer
    ObjectList build(Glossary&, Simul&);

    /// perform one Monte-Carlo step
    void       step();
    
    /// add interactions to a Meca
    void       setInteractions(Meca &) const;

#if ( DIM == 2 )
    /// get value of Q_target
    Matrix22   targetQ() const { return Q_target; }

    /// get value of Q_curr
    Matrix22   currentQ() const { return Q_curr; }

    /// get value of nemStress
    Matrix22   nematicStress() const { return nemStress; }
#endif

    /// get value of nemEnergy
    real       nematicEnergy() const { return nemEnergy; }

    /// display parameters
    PointDisp const* disp() const { return nullptr; }

    //------------------------------ read/write --------------------------------

    /// a unique character identifying the class in 
    static const ObjectTag TAG = 't';
    
    /// return unique character identifying the class
    ObjectTag       tag() const { return TAG; }
    
    /// return associated Property
    Property const* property() const { return prop; }
    
 };

#endif

